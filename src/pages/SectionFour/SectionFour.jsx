import React from 'react';
import './SectionFour.scss';

import Imgag from '../../assets/img/imgSectionFour.png'

function SectionFour() {
    return(
        <div className='fourthSection'>
            <div className='lineSectionFour'></div>
            <div className='characteristics'>
                <p className='characteristicsTitle'>
                    Характеристики
                </p>
                <div className='characteristicsDescription'>
                    <div className='characteristicsLine'></div>
                    <p>
                        У будівництві БЦ Bionic<br/>
                        використовуються сучасні<br/>
                        технології та надійні матеріали
                    </p>
                </div>
            </div>
            <div className='characteristicsBlock'>
                <div className='blockSectionFour'>
                    <p className='blockTitle'>89 950 м2</p>
                    <p className='blockDescription'>Загальна площа</p>
                </div>
                <div className='blockSectionFour'>
                    <p className='blockTitle'>89 950 м2</p>
                    <p className='blockDescription'>Загальна орендована площа</p>
                </div>
                <div className='blockSectionFour'>
                    <p className='blockTitle'>4-10</p>
                    <p className='blockDescription'>Загальна площа</p>
                </div>
                <div className='blockSectionFour'>
                    <p className='blockTitle'>A</p>
                    <p className='blockDescription'>Загальна площа</p>
                </div>
                <div className='blockSectionFour'>
                    <p className='blockTitle'>Відкрите</p>
                    <p className='blockDescription'>Тип планування</p>
                </div>
                <div className='blockSectionFour'>
                    <p className='blockTitle'>1800 м2</p>
                    <p className='blockDescription'>Середня площа поверху</p>
                </div>
                <div className='blockSectionFour'>
                    <p className='blockTitle'>350</p>
                    <p className='blockDescription'>Паркувальних місць</p>
                </div>
                <div className='blockSectionFour'>
                    <p className='blockTitle'>IV кв. 2020</p>
                    <p className='blockDescription'>Строк здачі</p>
                </div>
            </div>
            <div className='imgSectionFour'>
                <img src={Imgag}></img>
            </div>
        </div>
    )
}

export default SectionFour;