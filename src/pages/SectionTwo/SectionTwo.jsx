import React from 'react';
import Img from '../../assets/img/imgSectionTwo.png';
import Img2 from '../../assets/img/rectangleImage.png'

import './SectionTwo.scss'

function SectionTwo() {
    return (
        <div className='sectionTwo'>
            <div className='lineSectionTwo'></div>
            <div className='img'>
                <img src={Img}></img>
            </div>
            <div className='aboutBionicSectionTwo'>
                <div className='bionicSectionTwo'>
                    <p >Про БЦ Bionic</p>
                </div>
                <div>
                    <p className='textAboutBionic'>
                        Бізнес-центр Bionic — це семиповерхова будівля загальною площею 88<br/>
                        950 м2. На цокольному і першому поверхах розташовані заклади <br/>
                        громадського харчування і торгівлі, на 2-7 поверхах - офісні приміщення. <br/>
                        Сучасна архітектура і висока технологічність, відповідають високому <br/>
                        класу будівництва - задовольняють потреби різних груп бізнесу.<br/>
                        <br/>
                        Зручна транспорта розв’язка та насичена інфраструктура поруч робить <br/>
                        комплекс Bionic ідеальним варіантом для подальшої здачі приміщень в <br/>
                        аренду
                    </p>
                    <div className='moreAboutTheProject'>
                        <p>Більше про проект</p>
                    </div>
                </div>

             </div>
            <div className='img2Section2'>
                <div className='textNearImage'>
                    <div className='line2SectionTwo'></div>
                    <div className='textNearLine'>
                        <p>Цей проєкт унікальний<br/>
                            для Києва. У ньому<br/>
                            поєднуються і зручне<br/>
                            розташування, й тихе<br/>
                            спокійне місце</p>
                    </div>
                </div>
                <div>
                    <img src={Img2}></img>
                </div>
            </div>

        </div>
    )
}

export default SectionTwo;