import React from 'react';

function Title() {
    return(
        <div className='title-bionic'>
            <div className='whiteLine'></div>
            <div className='title'>Bionic</div>
            <div className='subtitle'>
                <p>
                    Бізнес-центр класу А в Голосіївському районі<br/>
                    Києва. Продаж офісних, торгових та складських<br/>
                    приміщень від 25 000 грн / м
                </p>
            </div>
            <div className='button'>
                <div className='firstBTN'>
                    <p className='first'>Переглянути приміщення</p>
                </div>
                <div className='secondBTN'>
                    <p className='second'>Зв’язатись з менеджером</p>
                </div>
            </div>
        </div>
    )
}

export default Title;