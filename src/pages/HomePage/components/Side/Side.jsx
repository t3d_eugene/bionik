import React from 'react';

function Side() {
    return (
        <div className='sidePanel'>
            <div className='address'>
                <p>
                    Киїів, вулиця Сумська, 1<br/>
                    метро Васильківська
                </p>
            </div>
            <div className='number'>
                <p>
                    044 123 23 23
                </p>
            </div>
            <div className='callback'>
                <p>
                    Замовити дзвінок
                </p>
            </div>
            <div className='sideLine'></div>
        </div>
    )
}

export default Side;